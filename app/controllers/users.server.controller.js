var User = require('mongoose').model('User');

exports.insertUser = function(user){
	var user = new User(user);
	user.save(function(err){
	if(err){console.log(err)}
	else{console.log('insertion successful')}});
};
exports.create = function(req, res,next){
 	console.log("create");
	console.log(req.body);

	var user = new User(req.body);
	user.save(function(err,user){
	if(err){
		  return next(err);
	}else{
	res.json(user);
	}
   }); 
};

exports.list = function(req,res,next){
	User.find({},'username email',function(err,users){
	if(err){
		return next(err);
	}else{
		res.json(users);
	}});
};


exports.read = function(req,res){
	console.log("users.read()");
	res.json(req.user);
};

exports.userByID = function(req,res,next,id){
		
	//console.log("I am here at userByID");
	User.findById(id, function(err,user){
	 		if(err){
				//console.log("error");
				//console.log(err);
				return next(err);
			  }else{
				//console.log("user");
				//console.log(user);
				req.user = user;
				next();
			 }	
	});
}; 

exports.update = function(req, res,next){

	console.log("users.update");

	User.findByIdAndUpdate(req.user.id,req.body,function(err,user){
	if(err){
		return next(err);
	}else{
	res.json(user);
	}
});	
};

exports.delete = function(req ,res , next){
	req.user.remove(function(err){
		if(err){
		return next(err);
		}else{
		    res.json(req.user);
		}
	   });
};
