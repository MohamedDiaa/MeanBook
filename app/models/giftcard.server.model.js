var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var cardSchema = new Schema({
	'value': Number,
 	'price': Number,
	'brand':String,
	'store' : String,
	'created':{
	    'type': Date,
	    'default': Date.now
	},
  'validUntil': Date
  });

cardSchema.set('toJSON' ,{getters: true, virtuals:true});

mongoose.model('GiftCard', cardSchema);
