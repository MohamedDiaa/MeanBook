var cards = require('../../app/controllers/giftcards.server.controller');

module.exports = function(app){

    app.route('/cards').post(cards.create).get(cards.list);

  //  app.route('/users/:userID').get(users.read).put(users.update).delete(users.delete);
  //  app.param('userID' , users.userByID);
	};
