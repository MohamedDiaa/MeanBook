var config = require('./config'),
    mongoose = require('mongoose');

module.exports = function(){

	//mongoose.connect(database.remoteUrl);
	var db = mongoose.connect(config.db);

	var dbm =  mongoose.connection;

	dbm.once('open',function(){});

	require('../app/models/user.server.model');
  require('../app/models/giftcard.server.model');
	

	return db;
};
